
/*
   Include files needed for the PBJacobi preconditioner:
     pcimpl.h - private include file intended for use by all preconditioners
*/

#include <petsc/private/pcimpl.h>   /*I "petscpc.h" I*/

/*
   Private context (data structure) for the PBJacobi preconditioner.
*/
typedef struct {
  const MatScalar *diag;
  PetscInt        bs,mbs;
} PC_PBJacobi;


static PetscErrorCode PCApply_PBJacobi_N(PC pc,Vec x,Vec y)
{
  PC_PBJacobi     *jac = (PC_PBJacobi*)pc->data;
  PetscErrorCode  ierr;
  PetscInt        i,ib,jb;
  const PetscInt  m = jac->mbs;
  const PetscInt  bs = jac->bs;
  const MatScalar *diag = jac->diag;
  PetscScalar       *yy;
  const PetscScalar *xx;

  PetscFunctionBegin;
  ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
  ierr = VecGetArray(y,&yy);CHKERRQ(ierr);
  for (i=0; i<m; i++) {
    for (ib=0; ib<bs; ib++){
      PetscScalar rowsum = 0;
      for (jb=0; jb<bs; jb++){
        rowsum += diag[ib+jb*bs] * xx[bs*i+jb];
      }
      yy[bs*i+ib] = rowsum;
    }
    diag += bs*bs;
  }
  ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
  ierr = VecRestoreArray(y,&yy);CHKERRQ(ierr);
  ierr = PetscLogFlops(2*bs*bs-bs);CHKERRQ(ierr); /* 2*bs2 - bs */
  PetscFunctionReturn(0);
}
/* -------------------------------------------------------------------------- */
static PetscErrorCode PCSetUp_PBJacobi(PC pc)
{
  PC_PBJacobi    *jac = (PC_PBJacobi*)pc->data;
  PetscErrorCode ierr;
  Mat            A = pc->pmat;
  MatFactorError err;
  PetscInt       nlocal;
  
  PetscFunctionBegin;
  ierr = MatInvertBlockDiagonal(A,&jac->diag);CHKERRQ(ierr);
  ierr = MatFactorGetError(A,&err);CHKERRQ(ierr);
  if (err) pc->failedreason = (PCFailedReason)err;
 
  ierr = MatGetBlockSize(A,&jac->bs);CHKERRQ(ierr);
  ierr = MatGetLocalSize(A,&nlocal,NULL);CHKERRQ(ierr);
  jac->mbs = nlocal/jac->bs;
  pc->ops->apply = PCApply_PBJacobi_N;
  PetscFunctionReturn(0);
}
/* -------------------------------------------------------------------------- */
static PetscErrorCode PCDestroy_PBJacobi(PC pc)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /*
      Free the private data structure that was hanging off the PC
  */
  ierr = PetscFree(pc->data);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode PCView_PBJacobi(PC pc,PetscViewer viewer)
{
  PetscErrorCode ierr;
  PC_PBJacobi    *jac = (PC_PBJacobi*)pc->data;
  PetscBool      iascii;

  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscViewerASCIIPrintf(viewer,"  point-block size %D\n",jac->bs);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* -------------------------------------------------------------------------- */
/*MC
     PCPBJACOBI - Point block Jacobi preconditioner


   Notes: See PCJACOBI for point Jacobi preconditioning

   This works for AIJ and BAIJ matrices and uses the blocksize provided to the matrix

   Uses dense LU factorization with partial pivoting to invert the blocks; if a zero pivot
   is detected a PETSc error is generated.

   Developer Notes: This should support the PCSetErrorIfFailure() flag set to PETSC_TRUE to allow
   the factorization to continue even after a zero pivot is found resulting in a Nan and hence
   terminating KSP with a KSP_DIVERGED_NANORIF allowing
   a nonlinear solver/ODE integrator to recover without stopping the program as currently happens.

   Developer Note: Perhaps should provide an option that allows generation of a valid preconditioner
   even if a block is singular as the PCJACOBI does.

   Level: beginner

  Concepts: point block Jacobi


.seealso:  PCCreate(), PCSetType(), PCType (for list of available types), PC, PCJACOBI

M*/

PETSC_EXTERN PetscErrorCode PCCreate_PBJacobi(PC pc)
{
  PC_PBJacobi    *jac;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /*
     Creates the private data structure for this preconditioner and
     attach it to the PC object.
  */
  ierr     = PetscNewLog(pc,&jac);CHKERRQ(ierr);
  pc->data = (void*)jac;

  /*
     Initialize the pointers to vectors to ZERO; these will be used to store
     diagonal entries of the matrix for fast preconditioner application.
  */
  jac->diag = 0;

  /*
      Set the pointers for the functions that are provided above.
      Now when the user-level routines (such as PCApply(), PCDestroy(), etc.)
      are called, they will automatically call these functions.  Note we
      choose not to provide a couple of these functions since they are
      not needed.
  */
  pc->ops->apply               = 0; /*set depending on the block size */
  pc->ops->applytranspose      = 0;
  pc->ops->setup               = PCSetUp_PBJacobi;
  pc->ops->destroy             = PCDestroy_PBJacobi;
  pc->ops->setfromoptions      = 0;
  pc->ops->view                = PCView_PBJacobi;
  pc->ops->applyrichardson     = 0;
  pc->ops->applysymmetricleft  = 0;
  pc->ops->applysymmetricright = 0;
  PetscFunctionReturn(0);
}


